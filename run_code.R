# This file contains the code that can be used to recreate figures
# and simulations from the paper.  Keep in mind that this code was 
# originally written in a different form so they processes could be
# run in parallel on a large computing grid.  Thus, this is simplified
# and contains fewer simulations than what was run in the paper.
# 
# Please send questions to jeanette.mumford@gmail.com
# created: 7/23/2014, Jmumford

# For each example, make sure to type in your root.dir (directory 
# containing all files)

##############################
##### Figure 2 #################

rm(list=ls())
root.dir = "//Users/jeanettemumford/Documents/Research/Projects/RSA_eff/Code_public"
source(sprintf("%s/rsa_functions.R", root.dir))
library(fields)
nvox=500
ntrials=30
lambda = 1.5  #(lam = 1.5 and T=3 yields avg ISI of 1.03s)
T=3
shift = 2 # ISI between shift to shift+T
len.stim=rep(2, 2*ntrials)
noise.sd=1

type.trialord = 1  #1=block, 2=alternating, 3= random
lab.output = makeLabels(ntrials, type.trialord)
trial.nums = lab.output$trial.nums
trial.cor.labels = lab.output$trial.cor.labels
trial.move = lab.output$trial.move

move.2.blocks = c(which(trial.nums==1), which(trial.nums==2))
des.gen = make.des.mat.bias.texp(ntrials,lambda, T, shift , len.stim, hrf.in)
desmat=des.gen$des.mat
desmat.mc=mn.ctr.column(desmat)
ntpts = dim(desmat)[1]
cormat=diag(rep(1, 2*ntrials))
v.y = diag(rep(1, ntpts))
true.cor=calc.cor.wtemp.cov(desmat.mc, desmat, ntrials, trial.nums, cormat, v.y)        
lss.des.cor=true.cor$lss.cor
lsa.des.cor=true.cor$lsa.cor

min.rng=-0.4
set.panel()
par(oma=c(0,0,0,4), mar=c(5, 5, 4.1, .5))
set.panel(1,3)
brks.neg = seq(from=min.rng, to = -0.001, by = 0.03)
brks.pos = c(seq(from=0.001, to = 1, by = 0.03), 1)
nbrks.neg = length(brks.neg)
nbrks.pos = length(brks.pos)
brks = c(brks.neg, brks.pos )
zero.point = max(which(brks <= 0))
nbreaks = length(brks)
cols.neg=colorRamp(c("blue", "lightblue"))((0:(zero.point))/(zero.point))
t.rng=c(1:(ntrials*2))
cols.pos = colorRamp(c("#FFF7F7", "#FF0000"))((0:(nbreaks -zero.point+1))/(nbreaks -zero.point+1))
cols.rgb = rbind(cols.neg[c(1:(zero.point-1)),],cbind(255,255,255), cols.pos[4:(nbreaks - zero.point+2),])
cols = rgb(cols.rgb, maxColorValue=255)
image(t.rng, t.rng, cormat[move.2.blocks, move.2.blocks], xlab="Trial Number", ylab="Trial Number", main="Truth" , breaks=brks, col=cols, cex.main=2, cex.lab=2)
image(t.rng, t.rng,lsa.des.cor[move.2.blocks, move.2.blocks], xlab="Trial Number", ylab="Trial Number", main="LSA" , breaks=brks, col=cols, cex.main=2, cex.lab=2)
image(t.rng, t.rng, lss.des.cor[move.2.blocks, move.2.blocks], xlab="Trial Number", ylab="Trial Number", main="LSS" , breaks=brks, col=cols, cex.main=2, cex.lab=2)
par(oma=c(0,0,0,1))
image.plot(legend.only=TRUE, zlim=c(min.rng, 1), col = cols)
set.panel()


#################################################
#################  Figure 3 #####################

###########################
# Figure 3, upper left hand panel: Without temporal autocorrelation, 3sISI

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))
library(ggplot2)

mn.isi = 3
nsubs = 30
blocked.3s = pattern.sim.no.cov(mn.isi, 1, nsubs)
alternating.3s = pattern.sim.no.cov(mn.isi, 2, nsubs)
random.3s = pattern.sim.no.cov(mn.isi, 3, nsubs)

diff = c(c(blocked.3s$sub.diff.avgs.lsa),
         c(blocked.3s$sub.diff.avgs.lss),
         c(alternating.3s$sub.diff.avgs.lsa),
         c(alternating.3s$sub.diff.avgs.lss),
         c(random.3s$sub.diff.avgs.lsa),
         c(random.3s$sub.diff.avgs.lss))         
est.type.ord = factor(rep(c("Blocked (3s ISI)", "Alternating (3s ISI)", "Random (3s ISI)"), each = 180), levels = c("Blocked (3s ISI)", "Alternating (3s ISI)", "Random (3s ISI)"), ordered= "TRUE")
diff.type = factor(rep(rep(c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2"), each = 30), 6), levels = c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2"), ordered = "TRUE")
estimator.type = rep(rep(c("LSA", "LSS"), each = 90), 3)

dat.plot = data.frame(diff, est.type.ord, diff.type, estimator.type)

qplot(estimator.type, diff, fill = diff.type, data = dat.plot, 
       geom = "boxplot", position = 'dodge') + theme_bw(base_size=16) + 
       labs(x = "Pattern Estimator", y= "Difference")+
       facet_wrap(~est.type.ord, nrow=1, ncol=3)+
       scale_fill_manual(name="Paired \nDifference", values=rainbow(3) )+
       geom_abline(intercept = 0, slope = 0, colour="slategray4", lty=2)+
       labs(title = "Without temporal autocorrelation") 

##############################
# Figure 3, upper right hand panel: Without temporal autocorrelation, 7sISI

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))
library(ggplot2)

mn.isi = 7
nsubs = 30
blocked.7s = pattern.sim.no.cov(mn.isi, 1, nsubs)
alternating.7s = pattern.sim.no.cov(mn.isi, 2, nsubs)
random.7s = pattern.sim.no.cov(mn.isi, 3, nsubs)

diff = c(c(blocked.7s$sub.diff.avgs.lsa),
         c(blocked.7s$sub.diff.avgs.lss),
         c(alternating.7s$sub.diff.avgs.lsa),
         c(alternating.7s$sub.diff.avgs.lss),
         c(random.7s$sub.diff.avgs.lsa),
         c(random.7s$sub.diff.avgs.lss))
est.type.ord = factor(rep(c("Blocked (7s ISI)", "Alternating (7s ISI)", "Random (7s ISI)"), each = 180), levels = c("Blocked (7s ISI)", "Alternating (7s ISI)", "Random (7s ISI)"), ordered= "TRUE")
diff.type = factor(rep(rep(c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2"), each = 30), 6), levels = c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2"), ordered = "TRUE")
estimator.type = rep(rep(c("LSA", "LSS"), each = 90), 3)

dat.plot = data.frame(diff, est.type.ord, diff.type, estimator.type)

qplot(estimator.type, diff, fill = diff.type, data = dat.plot, 
       geom = "boxplot", position = 'dodge') + theme_bw(base_size=16) + 
       labs(x = "Pattern Estimator", y= "Difference")+
       facet_wrap(~est.type.ord, nrow=1, ncol=3)+
       scale_fill_manual(name="Paired \nDifference", values=rainbow(3) )+
       geom_abline(intercept = 0, slope = 0, colour="slategray4", lty=2)+
       labs(title = "Without temporal autocorrelation") 


###########################
# Figure 3, bottom left hand panel: With temporal autocorrelation, 3sISI

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))
library(ggplot2)

mn.isi = 3
nsubs = 30
same.des.all.subs = 0 # Use a different design for each subject
blocked.3s = pattern.sim.yes.cov(mn.isi, 1, nsubs, root.dir, same.des.all.subs)
alternating.3s = pattern.sim.yes.cov(mn.isi, 2, nsubs, root.dir, same.des.all.subs)
random.3s = pattern.sim.yes.cov(mn.isi, 3, nsubs, root.dir, same.des.all.subs)

diff = c(c(blocked.3s$sub.diff.avgs.lsa),
         c(blocked.3s$sub.diff.avgs.lss),
         c(alternating.3s$sub.diff.avgs.lsa),
         c(alternating.3s$sub.diff.avgs.lss),
         c(random.3s$sub.diff.avgs.lsa),
         c(random.3s$sub.diff.avgs.lss))         
est.type.ord = factor(rep(c("Blocked (3s ISI)", "Alternating (3s ISI)", "Random (3s ISI)"), each = 180), levels = c("Blocked (3s ISI)", "Alternating (3s ISI)", "Random (3s ISI)"), ordered= "TRUE")
diff.type = factor(rep(rep(c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2"), each = 30), 6), levels = c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2"), ordered = "TRUE")
estimator.type = rep(rep(c("LSA", "LSS"), each = 90), 3)

dat.plot = data.frame(diff, est.type.ord, diff.type, estimator.type)

qplot(estimator.type, diff, fill = diff.type, data = dat.plot, 
       geom = "boxplot", position = 'dodge') + theme_bw(base_size=16) + 
       labs(x = "Pattern Estimator", y= "Difference")+
       facet_wrap(~est.type.ord, nrow=1, ncol=3)+
       scale_fill_manual(name="Paired \nDifference", values=rainbow(3) )+
       geom_abline(intercept = 0, slope = 0, colour="slategray4", lty=2)+
       labs(title = "With temporal autocorrelation") 

###########################
# Figure 3, bottom right hand panel: With temporal autocorrelation, 3sISI

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))
library(ggplot2)

mn.isi = 7
nsubs = 30
same.des.all.subs = 0 # different design for each subject
blocked.7s = pattern.sim.yes.cov(mn.isi, 1, nsubs, root.dir, same.des.all.subs)
alternating.7s = pattern.sim.yes.cov(mn.isi, 2, nsubs, root.dir, same.des.all.subs)
random.7s = pattern.sim.yes.cov(mn.isi, 3, nsubs, root.dir, same.des.all.subs)

diff = c(c(blocked.7s$sub.diff.avgs.lsa),
         c(blocked.7s$sub.diff.avgs.lss),
         c(alternating.7s$sub.diff.avgs.lsa),
         c(alternating.7s$sub.diff.avgs.lss),
         c(random.7s$sub.diff.avgs.lsa),
         c(random.7s$sub.diff.avgs.lss))         
est.type.ord = factor(rep(c("Blocked (7s ISI)", "Alternating (7s ISI)", "Random (7s ISI)"), each = 180), levels = c("Blocked (7s ISI)", "Alternating (7s ISI)", "Random (7s ISI)"), ordered= "TRUE")
diff.type = factor(rep(rep(c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2"), each = 30), 6), levels = c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2"), ordered = "TRUE")
estimator.type = rep(rep(c("LSA", "LSS"), each = 90), 3)

dat.plot = data.frame(diff, est.type.ord, diff.type, estimator.type)

qplot(estimator.type, diff, fill = diff.type, data = dat.plot, 
       geom = "boxplot", position = 'dodge') + theme_bw(base_size=16) + 
       labs(x = "Pattern Estimator", y= "Difference")+
       facet_wrap(~est.type.ord, nrow=1, ncol=3)+
       scale_fill_manual(name="Paired \nDifference", values=rainbow(3) )+
       geom_abline(intercept = 0, slope = 0, colour="slategray4", lty=2)+
       labs(title = "With temporal autocorrelation") 


#################################################
#################  Table 1 ######################


#WR/Ind (3s ISI)
#    &
#WR/Ind (7s ISI)

# In the paper we ran 10,000 simulations in parallel batches
# on a large compute grid.  This example is for a meager 100 
# samples, which is hardly enough for type I error calculations 
# thus, is included only for illustration.

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))

mn.isi = 3  #change to 7 for the 7s ISI simulations
nsubs = 5   # We use 30 subjects in the paper
nsims = 100  # again, we ran 10,000 (!) for the paper
            # so this will not yield the same results

pmat.block.lss = data.frame(matrix(0, nrow = nsims, ncol = 3))
names(pmat.block.lss)= c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2")
pmat.block.lsa = pmat.block.lss
pmat.alt.lss = pmat.block.lss
pmat.alt.lsa = pmat.block.lss
pmat.rand.lss = pmat.block.lss
pmat.rand.lsa = pmat.block.lss

for (i in 1:nsims){
  print(sprintf("Simulation %s", i))
  blocked = pattern.sim.no.cov(mn.isi, 1, nsubs)
  alternating = pattern.sim.no.cov(mn.isi, 2, nsubs)
  random = pattern.sim.no.cov(mn.isi, 3, nsubs)

  pmat.block.lss[i,] = blocked$p.lss
  pmat.block.lsa[i,] = blocked$p.lsa
  pmat.alt.lss[i,] = alternating$p.lss
  pmat.alt.lsa[i,] = alternating$p.lsa
  pmat.rand.lss[i,] = random$p.lss
  pmat.rand.lsa[i,] = random$p.lsa
}

#Note, the p-values are signed, so we could keep 
# track of the direction of the bias

# row 1
apply(abs(pmat.block.lsa)<=0.05, 2, mean)
apply(abs(pmat.alt.lsa)<=0.05, 2, mean)
apply(abs(pmat.rand.lsa)<=0.05, 2, mean)
# row 2
apply(abs(pmat.block.lss)<=0.05, 2, mean)
apply(abs(pmat.alt.lss)<=0.05, 2, mean)
apply(abs(pmat.rand.lss)<=0.05, 2, mean)

# To obtain rows 3 & 4, change mn.isi to 7 and repeat the above code


####################
#WR/Cor (3s ISI)
#    &
#WR/Cor (7s ISI)
#    &
#WR/Cor (15s ISI)

# In the paper we ran 10,000 simulations in parallel batches
# on a large compute grid.  This example is for a meager 100 
# samples, which is hardly enough for type I error calculations 
# thus, is included only for illustration.

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))

mn.isi = 3  #change to 7/15 for the 7s/15s ISI simulations
nsubs = 5   # We use 30 subjects in the paper
nsims = 100  # again, we ran 10,000 (!) for the paper
            # so this will not yield the same results

pmat.block.lss = data.frame(matrix(0, nrow = nsims, ncol = 3))
names(pmat.block.lss)= c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2")
pmat.block.lsa = pmat.block.lss
pmat.block.add6 = pmat.block.lss
pmat.alt.lss = pmat.block.lss
pmat.alt.lsa = pmat.block.lss
pmat.alt.add6 = pmat.block.lss
pmat.rand.lss = pmat.block.lss
pmat.rand.lsa = pmat.block.lss
pmat.rand.add6 = pmat.block.lss

for (i in 1:nsims){
  print(sprintf("Simulation %s", i))
  blocked = pattern.sim.yes.cov(mn.isi, 1, nsubs, root.dir, 0)
  alternating = pattern.sim.yes.cov(mn.isi, 2, nsubs, root.dir, 0)
  random = pattern.sim.yes.cov(mn.isi, 3, nsubs, root.dir, 0)

  pmat.block.lss[i,] = blocked$p.lss
  pmat.block.lsa[i,] = blocked$p.lsa
  pmat.block.add6[i,] = blocked.p.add6
  pmat.alt.lss[i,] = alternating$p.lss
  pmat.alt.lsa[i,] = alternating$p.lsa
  pmat.alt.add6[i,] = alternating$p.add6
  pmat.rand.lss[i,] = random$p.lss
  pmat.rand.lsa[i,] = random$p.lsa
  pmat.rand.add6[i,] = random$p.add6
}

#Note, the p-values are signed, so we could keep 
# track of the direction of the bias


# row 5
apply(abs(pmat.block.lsa)<=0.05, 2, mean)
apply(abs(pmat.alt.lsa)<=0.05, 2, mean)
apply(abs(pmat.rand.lsa)<=0.05, 2, mean)
# row 6
apply(abs(pmat.block.lss)<=0.05, 2, mean)
apply(abs(pmat.alt.lss)<=0.05, 2, mean)
apply(abs(pmat.rand.lss)<=0.05, 2, mean)

# add 6 (only included for ISI=15s)
apply(abs(pmat.block.add6)<=0.05, 2, mean)
apply(abs(pmat.alt.add6)<=0.05, 2, mean)
apply(abs(pmat.rand.add6)<=0.05, 2, mean)


# To obtain rows 7 & 8, and 9& 10 change mn.isi to 7 or 15 and repeat the above code

####################
#BR/Cor (same) (3s ISI)
#    &
#BR/Cor (different) (3s ISI)


rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))

mn.isi = 3   #change to 7/15 for the 7s/15s ISI simulations
nsubs = 5    # We use 30 subjects in the paper
nsims = 100   # again, we ran 10,000 (!) for the paper
             # so this will not yield the same results
nvox = 500  
same.des = 1 # set to 1, this uses the same design matrix for both runs
             # change to 0 if you want a different design for each run

pmat.block.lss = data.frame(matrix(0, nrow = nsims, ncol = 3))
names(pmat.block.lss)= c("wt1-wt2", "wt1-bt1t2", "wt2-bt1t2")
pmat.block.lsa = pmat.block.lss
pmat.alt.lss = pmat.block.lss
pmat.alt.lsa = pmat.block.lss
pmat.rand.lss = pmat.block.lss
pmat.rand.lsa = pmat.block.lss


for (i in 1:nsims){
  print(sprintf("Simulation %s", i))
  blocked = pattern.sim.yes.cov.btwn.run(mn.isi, 1, nsubs, 
                                         root.dir, nvox, same.des)
  alternating = pattern.sim.yes.cov.btwn.run(mn.isi, 2, nsubs, 
                                         root.dir, nvox, same.des)
  random = pattern.sim.yes.cov.btwn.run(mn.isi, 3, nsubs, 
                                         root.dir, nvox, same.des)

  pmat.block.lss[i,] = blocked$p.lss
  pmat.block.lsa[i,] = blocked$p.lsa
  pmat.alt.lss[i,] = alternating$p.lss
  pmat.alt.lsa[i,] = alternating$p.lsa
  pmat.rand.lss[i,] = random$p.lss
  pmat.rand.lsa[i,] = random$p.lsa
}

#Note, the p-values are signed, so we could keep 
# track of the direction of the bias

# row 12
apply(abs(pmat.block.lsa)<=0.05, 2, mean)
apply(abs(pmat.alt.lsa)<=0.05, 2, mean)
apply(abs(pmat.rand.lsa)<=0.05, 2, mean)

# row 13
apply(abs(pmat.block.lss)<=0.05, 2, mean)
apply(abs(pmat.alt.lss)<=0.05, 2, mean)
apply(abs(pmat.rand.lss)<=0.05, 2, mean)

# To get rows 14 and 15, set same.des=0


##########################
# Table 2

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))

mn.isi = 3  #change to 7/15 for the 7s/15s ISI simulations
nsubs = 5   # We use 30 subjects in the paper
nsims = 100  # again, we ran 10,000 (!) for the paper
            # so this will not yield the same results
same.des.all.subs = 1 # Use the same design for all subjects

pmat.rand.lss = data.frame(matrix(0, nrow = nsims, ncol = 3))
pmat.rand.lsa = pmat.rand.lss

for (i in 1:nsims){
  print(sprintf("Simulation %s", i))
  random = pattern.sim.yes.cov(mn.isi, 3, nsubs, root.dir, same.des.all.subs)

  pmat.rand.lss[i,] = random$p.lss
  pmat.rand.lsa[i,] = random$p.lsa
}

apply(abs(pmat.rand.lsa)<=0.05, 2, mean)
apply(abs(pmat.rand.lss)<=0.05, 2, mean)



###########################
# SVM (Figure 4)

# within-run Blocked/Alt/Rand (3 & 7s)

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))
library(ggplot2)

nvox = 500
mn.isi = 3   #Change to 7 for the 7s ISI
run.btwn.sub.svm = 0  #set this to 1 for between-sub svm
nruns.win = 1         # 1=win run and 2=btwn run
nsubs = 1000

perm.trials = 2  # set to 1 if you want the trial order permuted differently
                 # for each subject (random) and 2 if you want 1's and 2's 
                 #randomly swapped (block and alte)
type.trialord = 1 # (1=block, 2=alternating, 3=random)

#Block
svm.out.blk = run.rsa.svm.tempcov(nvox,  mn.isi , nsubs,  nruns.win, type.trialord, perm.trials, run.btwn.sub.svm)
dat.block= data.frame(svm.out.blk$win.sub.acc.lss, svm.out.blk$win.sub.acc.lsa)
names(dat.block)= c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2")

# Alternating
type.trialord = 2
svm.out.alt = run.rsa.svm.tempcov(nvox,  mn.isi , nsubs,  nruns.win, type.trialord, perm.trials, run.btwn.sub.svm)
dat.alt= data.frame(svm.out.alt$win.sub.acc.lss, svm.out.alt$win.sub.acc.lsa)
names(dat.alt)= c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2")

# Random
type.trialord = 3
perm.trials = 1
svm.out.rand = run.rsa.svm.tempcov(nvox,  mn.isi , nsubs,  nruns.win, type.trialord, perm.trials, run.btwn.sub.svm)
dat.rand= data.frame(svm.out.rand$win.sub.acc.lss, svm.out.rand$win.sub.acc.lsa)
names(dat.rand)= c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2")

trial.ord.all = rep(c("Block", "Alternating", "Random"), each = nsubs)
model = rep(rep(c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2"), each = nsubs), 3)
dat.all = c(c(as.matrix(dat.block)), c(as.matrix(dat.alt)), c(as.matrix(dat.rand)))

dat.plot = data.frame(dat.all, trial.ord.all, model)

qplot(trial.ord.all, dat.all, fill = model, data = dat.plot, 
         geom = "boxplot", position = 'dodge') + theme_bw(base_size=16) + 
       labs(x = "Pattern Estimator", y= "Accuracy")

# To obtain 7s, repeat, but change mn.isi to 7


##################################
# between-run(same)  Blocked/Alt/Rand (3 & 7s)

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))
library(ggplot2)

nvox = 500
mn.isi = 3   #Change to 7 for the 7s ISI
run.btwn.sub.svm = 0  #set this to 1 for between-sub svm
nruns.win = 2         # 1=win run and 2=btwn run
nsubs = 1000

perm.trials = 2  # set to 1 if you want the trial order permuted differently
                 # for each subject (random) and 2 if you want 1's and 2's 
                 #randomly swapped (block and alte)
type.trialord = 1 # (1=block, 2=alternating, 3=random)

#Block
svm.out.blk = run.rsa.svm.tempcov(nvox,  mn.isi , nsubs,  nruns.win, type.trialord, perm.trials, run.btwn.sub.svm)
dat.block= data.frame(svm.out.blk$win.sub.acc.lss, svm.out.blk$win.sub.acc.lsa)
names(dat.block)= c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2")

# Alternating
type.trialord = 2
svm.out.alt = run.rsa.svm.tempcov(nvox,  mn.isi , nsubs,  nruns.win, type.trialord, perm.trials, run.btwn.sub.svm)
dat.alt= data.frame(svm.out.alt$win.sub.acc.lss, svm.out.alt$win.sub.acc.lsa)
names(dat.alt)= c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2")

# Random
type.trialord = 3
perm.trials = 1
svm.out.rand = run.rsa.svm.tempcov(nvox,  mn.isi , nsubs,  nruns.win, type.trialord, perm.trials, run.btwn.sub.svm)
dat.rand= data.frame(svm.out.rand$win.sub.acc.lss, svm.out.rand$win.sub.acc.lsa)
names(dat.rand)= c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2")

trial.ord.all = rep(c("Block", "Alternating", "Random"), each = nsubs)
model = rep(rep(c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2"), each = nsubs), 3)
dat.all = c(c(as.matrix(dat.block)), c(as.matrix(dat.alt)), c(as.matrix(dat.rand)))

dat.plot = data.frame(dat.all, trial.ord.all, model)

qplot(trial.ord.all, dat.all, fill = model, data = dat.plot, 
         geom = "boxplot", position = 'dodge') + theme_bw(base_size=16) + 
       labs(x = "Pattern Estimator", y= "Accuracy")

# To obtain 7s, repeat, but change mn.isi to 7

##################################
# between-run(diff)  Blocked/Alt/Rand (3 & 7s)

rm(list = ls())
root.dir = "/corral-repl/utexas/poldracklab/users/mumford/Projects/RSA_eff/Code/Code_share"
source(sprintf("%s/rsa_functions.R", root.dir))
library(ggplot2)

nvox = 500
mn.isi = 3   #Change to 7 for the 7s ISI
run.btwn.sub.svm = 1  #set this to 1 for between-"sub" svm
nruns.win = 1         # 1=win run and 2=btwn run
nsubs = 2  # This is misleading, keep at 2, it really means 2 runs
nsims = 1000

perm.trials = 2  # set to 1 if you want the trial order permuted differently
                 # for each subject (random) and 2 if you want 1's and 2's 
                 #randomly swapped (block and alte)
type.trialord = 1 # (1=block, 2=alternating, 3=random)

#Block
dat.save.all.block = data.frame(matrix(0, nrow = nsims, ncol = 4))
names(dat.save.all.block)= c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2")
for (i in 1:nsims){
  print(sprintf("Subject %s", i))
  svm.out = run.rsa.svm.tempcov(nvox,  mn.isi , nsubs,  nruns.win, type.trialord, perm.trials, run.btwn.sub.svm)
  dat.save.all.block[i,] = c(svm.out$btwn.sub.acc.lss, svm.out$btwn.sub.acc.lsa)
}

#Alternating
type.trialord = 2
dat.save.all.alternating = data.frame(matrix(0, nrow = nsims, ncol = 4))
names(dat.save.all.alternating)= c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2")
for (i in 1:nsims){
  print(sprintf("Subject %s", i))
  svm.out = run.rsa.svm.tempcov(nvox,  mn.isi , nsubs,  nruns.win, type.trialord, perm.trials, run.btwn.sub.svm)
  dat.save.all.alternating[i,] = c(svm.out$btwn.sub.acc.lss, svm.out$btwn.sub.acc.lsa)
}

# Random
type.trialord = 3
perm.trials = 1
dat.save.all.random = data.frame(matrix(0, nrow = nsims, ncol = 4))
names(dat.save.all.random)= c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2")
for (i in 1:nsims){
  print(sprintf("Subject %s", i))
  svm.out = run.rsa.svm.tempcov(nvox,  mn.isi , nsubs,  nruns.win, type.trialord, perm.trials, run.btwn.sub.svm)
  dat.save.all.random[i,] = c(svm.out$btwn.sub.acc.lss, svm.out$btwn.sub.acc.lsa)
}

trial.ord.all = rep(c("Block", "Alternating", "Random"), each = nsims)
model = rep(rep(c("LSS.T1", "LSS.T2", "LSA.T1", "LSA.T2"), each = nsims), 3)
dat.all = c(c(as.matrix(dat.save.all.block)), c(as.matrix(dat.save.all.alternating)), c(as.matrix(dat.save.all.random)))

dat.plot = data.frame(dat.all, trial.ord.all, model)

qplot(trial.ord.all, dat.all, fill = model, data = dat.plot, 
         geom = "boxplot", position = 'dodge') + theme_bw(base_size=16) + 
       labs(x = "Pattern Estimator", y= "Accuracy")

# To get 7s, go back and change mn.isi to 7